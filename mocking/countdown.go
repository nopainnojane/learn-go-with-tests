package main

import (
	"fmt"
	"io"
	"os"
	"time"
)

type Sleeper interface {
	Sleep()
}

type DefaultSleeper struct{}

func (d DefaultSleeper) Sleep() {
	time.Sleep(1 * time.Second)
}

func Countdown(out io.Writer, sleeper Sleeper) {
	for _, n := range []int{3, 2, 1} {
		sleeper.Sleep()
		fmt.Fprintln(out, n)
	}
	sleeper.Sleep()
	fmt.Fprint(out, "Go!")
}

func main() {
	Countdown(os.Stdout, DefaultSleeper{})
}
