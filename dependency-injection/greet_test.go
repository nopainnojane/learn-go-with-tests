package main

import (
	"bytes"
	"testing"
)

func TestGreet(t *testing.T) {
	buffer := bytes.Buffer{}
	Greet(&buffer, "Nessa Jane")

	got := buffer.String()
	want := "Hello, Nessa Jane"

	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}
